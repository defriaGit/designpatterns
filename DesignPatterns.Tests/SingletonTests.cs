using DesignPatterns.Singleton;
using Moq;
using NUnit.Framework;

namespace DesignPatterns.Tests
{
    [TestFixture]
    public class SingletonTests
    {
        [SetUp]
        public void Setup()
        {
            _capitals = new[] {"Tokyo", "33200000", "New York", "17800000", "Sao Paulo", "17700000"};
            _mockFileReader = new Mock<IFileReader>();
            Repository.FileReader = _mockFileReader.Object;
        }

        private string[] _capitals;
        private Mock<IFileReader> _mockFileReader;

        [Test]
        public void IsSingletonTest()
        {
            _mockFileReader.Setup(fr => fr.Read(It.IsAny<string>())).Returns(_capitals);


            var db = Repository.Instance;
            var db2 = Repository.Instance;


            Assert.That(db, Is.SameAs(db2));
            Assert.That(Repository.Count, Is.EqualTo(1));
        }

        [Test]
        public void SingletonTotalPopulationTest()
        {
            _mockFileReader.Setup(fr => fr.Read(It.IsAny<string>())).Returns(_capitals);

            var rf = new SingletonRecordFinder();
            var names = new [] {"Tokyo", "New York"};

            var tp = rf.GetTotalPopulation(names);

            Assert.That(tp, Is.EqualTo(33200000 + 17800000));
        }
    }
}