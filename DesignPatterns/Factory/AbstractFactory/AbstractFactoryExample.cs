﻿namespace DesignPatterns.Factory.AbstractFactory
{
    internal class AbstractFactoryExample
    {
        public static void Example1()
        {
            var machine = new HotDrinkMachine();
            var drink = machine.MakeDrink();
        }
    }
}