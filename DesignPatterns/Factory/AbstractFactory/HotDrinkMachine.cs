﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.Factory.AbstractFactory
{
    public class HotDrinkMachine
    {
        //public enum AvailableDrink
        //{
        //    Coffee,
        //    Tea
        //}

        //private Dictionary<AvailableDrink, IHotDrinkFactory> _factories = new Dictionary<AvailableDrink, IHotDrinkFactory>();

        //public HotDrinkMachine()
        //{
        //    foreach (AvailableDrink drink in Enum.GetValues(typeof(AvailableDrink)))
        //    {
        //        var factory = (IHotDrinkFactory) Activator.CreateInstance(
        //            Type.GetType("DesignPatterns." + Enum.GetName(typeof(AvailableDrink), drink)));

        //        _factories.Add(drink, factory);
        //    }
        //}

        //public IHotDrink MakeDrink(AvailableDrink drink, int amount)
        //{
        //    return _factories[drink].Prepare(amount);
        //}

        private readonly List<Tuple<string, IHotDrinkFactory>> _factories = new List<Tuple<string, IHotDrinkFactory>>();

        public HotDrinkMachine()
        {
            foreach (var type in typeof(HotDrinkMachine).Assembly.GetTypes())
                if (typeof(IHotDrinkFactory).IsAssignableFrom(type) && !type.IsInterface)
                    _factories.Add(Tuple.Create(
                        type.Name.Replace("Factory", string.Empty),
                        (IHotDrinkFactory) Activator.CreateInstance(type)
                    ));
        }

        public IHotDrink MakeDrink()
        {
            Console.WriteLine("Available drinks: ");
            for (var index = 0; index < _factories.Count; index++)
            {
                var tuple = _factories[index];

                Console.WriteLine($"{index}: {tuple.Item1}");
            }

            while (true)
            {
                var s = Console.ReadLine();

                if (s != null
                    && int.TryParse(s, out int i)
                    && i >= 0
                    && i < _factories.Count)
                {
                    Console.Write("Specify amount: ");
                    s = Console.ReadLine();
                    if (s != null
                        && int.TryParse(s, out int amount)
                        && amount > 0)
                        return _factories[i].Item2.Prepare(amount);
                }
                Console.WriteLine("Incorrect input, try again!");
            }
        }
    }
}