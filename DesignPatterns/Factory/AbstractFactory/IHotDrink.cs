﻿namespace DesignPatterns.Factory.AbstractFactory
{
    public interface IHotDrink
    {
        void Consume();
    }
}