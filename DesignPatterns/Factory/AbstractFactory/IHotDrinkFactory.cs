﻿namespace DesignPatterns.Factory.AbstractFactory
{
    internal interface IHotDrinkFactory
    {
        IHotDrink Prepare(int amount);
    }
}