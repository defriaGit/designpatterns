﻿using System;

namespace DesignPatterns.Factory.AbstractFactory
{
    internal class Coffee : IHotDrink
    {
        public void Consume()
        {
            Console.WriteLine("This coffee is sensatinal!");
        }
    }
}