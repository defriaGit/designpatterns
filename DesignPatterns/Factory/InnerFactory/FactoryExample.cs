﻿using System;

namespace DesignPatterns.Factory.InnerFactory
{
    public class FactoryExample
    {
        public static void Example1()
        {
            var point = Point.Factory.NewPolarPoint(1.0, Math.PI / 2);
            Console.WriteLine(point);
        }
    }
}