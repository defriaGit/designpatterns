﻿using System;

namespace DesignPatterns.FacetedBulder
{
    class FacetedBuilderExample
    {
        public static void Example1()
        {
            var pb = new PersonBuilder();
            Person person = pb
                .Lives.At("123 London Road")
                .In("London")
                .WithPostcode("SW213")
                .Works.At("Fabrikam")
                .AsA("Engineer")
                .Earning(123000);

            Console.WriteLine(person);

        }
    }
}