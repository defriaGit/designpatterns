﻿using System;

namespace DesignPatterns.LiskovSubstitutionPrinciple
{
    internal class ExampleLiskovSubstitution
    {
        public static int Area(Rectangle rectangle) => rectangle.Width * rectangle.Height;

        public static void Example1()
        {
            var rectangle = new Rectangle(2, 3);
            Console.WriteLine($"{rectangle} has area {Area(rectangle)}");

            Rectangle square = new Square();
            square.Height = 4;
            Console.WriteLine($"{square} has area {Area(square)}");
        }
    }
}