﻿namespace DesignPatterns.LiskovSubstitutionPrinciple
{
    // Liskov Substitution Principle
    // Barbara Liskov, 
    // You should be able to substitute a Base type for  a subtype

    class Rectangle
    {
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }

        public Rectangle()
        {
        }

        public Rectangle(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public override string ToString()
        {
            return $"{nameof(Width)}: {Width}, {nameof(Height)}: {Height}";
        }
    }
}