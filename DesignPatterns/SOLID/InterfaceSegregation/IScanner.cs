﻿namespace DesignPatterns.InterfaceSegregation
{
    interface IScanner
    {
        void Scan(Document document);
    }
}