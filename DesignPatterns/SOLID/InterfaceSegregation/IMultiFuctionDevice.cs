﻿namespace DesignPatterns.InterfaceSegregation
{
    interface IMultiFuctionDevice : IScanner, IIPrinter, IFax
    {
        
    }
}