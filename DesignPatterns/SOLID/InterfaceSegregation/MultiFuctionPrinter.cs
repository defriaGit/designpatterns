﻿namespace DesignPatterns.InterfaceSegregation
{
    class MultiFuctionPrinter : IMultiFuctionDevice // correct form on interface segregation 
    {
        private readonly IIPrinter _printer;
        private readonly IScanner _scanner;
        private readonly IFax _fax;

        public MultiFuctionPrinter(IIPrinter printer, IScanner scanner, IFax fax)
        {
            _printer = printer;
            _scanner = scanner;
            _fax = fax;
        }

        public void Scan(Document document)
        {
            // decorator pattern
            _scanner.Scan(document);
           
        }

        public void Print(Document document)
        {
            _printer.Print(document);
        }

        public void Fax(Document document)
        {
            _fax.Fax(document);
        }
    }
}