﻿using System;

namespace DesignPatterns.InterfaceSegregation
{
    internal class Scanner : IScanner
    {
        public void Scan(Document document)
        {
            Console.WriteLine("Document scanned");
        }
    }
}