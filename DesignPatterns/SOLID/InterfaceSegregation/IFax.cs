﻿namespace DesignPatterns.InterfaceSegregation
{
    interface IFax
    {
        void Fax(Document document);
    }
}