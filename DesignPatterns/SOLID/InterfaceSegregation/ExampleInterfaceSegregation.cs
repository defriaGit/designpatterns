﻿namespace DesignPatterns.InterfaceSegregation
{
    class ExampleInterfaceSegregation
    {
        public static void Example1() // implement DI Container (autofac)
        {
            var printer = new MultiFuctionPrinter(new Printer(), new Scanner(), new Faxer());
            var document = new Document();
            printer.Fax(document);
            printer.Print(document);
            printer.Scan(document);
        }
    }
}