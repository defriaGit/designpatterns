﻿using System;

namespace DesignPatterns.InterfaceSegregation
{
    class Faxer : IFax
    {
        public void Fax(Document document)
        {
            Console.WriteLine("Document faxed");
        }
    }
}