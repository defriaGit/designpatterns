﻿using System;

namespace DesignPatterns.InterfaceSegregation
{
    class MultiFunctionPrinter : IIMachine 
    {
        public void Print(Document document)
        {
            Console.WriteLine("Document printed");
        }

        public void Scan(Document document)
        {
            Console.WriteLine("Document scanned");

        }

        public void Fax(Document document)
        {
            Console.WriteLine("Document faxed");

        }
    }
}