﻿using System;

namespace DesignPatterns.InterfaceSegregation
{
    internal class Printer : IIPrinter
    {
        public void Print(Document document)
        {
            Console.WriteLine("Document printed");
        }
    }
}