﻿namespace DesignPatterns.InterfaceSegregation
{
    interface IIMachine
    {
        void Print(Document document);
        void Scan(Document document);
        void Fax(Document document);
    }
}