﻿namespace DesignPatterns.InterfaceSegregation
{
    interface IIPrinter
    {
        void Print(Document document);
    }
}