using System;

namespace DesignPatterns.InterfaceSegregation
{
    class OldFashingPrinter : IIMachine // Violating the interface segregation principle
    {
        public void Print(Document document)
        {
            Console.WriteLine("Document printed");

        }

        public void Scan(Document document)
        {
            throw new NotImplementedException();
        }

        public void Fax(Document document)
        {
            throw new NotImplementedException();
        }
    }
}