﻿namespace DesignPatterns.OpenClosePrinciple
{
    interface ISpecification<T>
    {
        bool IsSatisfied(T t);
    }
}