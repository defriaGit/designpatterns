﻿namespace DesignPatterns.OpenClosePrinciple
{
    class SizeSpecification : ISpecification<Product>
    {
        private readonly Size _size;

        public SizeSpecification(Size size)
        {
            this._size = size; 
        }
        public bool IsSatisfied(Product t)
        {
            return _size == t.Size;
        }
    }
}