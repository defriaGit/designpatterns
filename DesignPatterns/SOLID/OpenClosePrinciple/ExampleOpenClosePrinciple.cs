﻿using System;

namespace DesignPatterns.OpenClosePrinciple
{
    class ExampleOpenClosePrinciple
    {
        public static void Example1()
        {

            var apple = new Product("Apple", Color.Green, Size.Small);
            var tree = new Product("Tree", Color.Green, Size.Large);
            var house = new Product("House", Color.Blue, Size.Large);
            Product[] products = { apple, tree, house };

            var pf = new ProductFilter();
            Console.WriteLine("Green products (Old):");
            foreach (var product in pf.FilterByColor(products, Color.Green))
            {
                Console.WriteLine($" - {product.Name} is green");
            }
            Console.WriteLine();


            var filterProduct = new FilterProduct();

            Console.WriteLine("Green products (New)");
            var spec = new ColorSpecification(Color.Green);
            foreach (var product in filterProduct.Filter(products, spec))
            {
                Console.WriteLine($"- {product.Name} is green");
            }
            Console.WriteLine();


            Console.WriteLine("Small products (New)");
            var sizeSpec = new SizeSpecification(Size.Small);
            foreach (var product in filterProduct.Filter(products, sizeSpec))
            {
                Console.WriteLine($"- {product.Name} is Small");
            }
            Console.WriteLine();


            Console.WriteLine("Size & Color products (New)");
            var sizeAndColorSpec = new AndSpecification<Product>(new ColorSpecification(Color.Green), new SizeSpecification(Size.Large));
            foreach (var product in filterProduct.Filter(products, sizeAndColorSpec))
            {
                Console.WriteLine($"- {product.Name} is Green and Large");
            }
            Console.WriteLine();
        }
    }
}
