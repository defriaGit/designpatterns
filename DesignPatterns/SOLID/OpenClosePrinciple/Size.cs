﻿namespace DesignPatterns.OpenClosePrinciple
{
    enum Size
    {
        Small, Medium, Large, Huge
    }
}