﻿using System.Collections.Generic;

namespace DesignPatterns.OpenClosePrinciple
{
    interface IFilter<T>
    {
        IEnumerable<T> Filter(IEnumerable<T> items, ISpecification<T> spec);
    }
}