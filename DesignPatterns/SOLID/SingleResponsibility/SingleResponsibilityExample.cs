﻿using System;
using System.Diagnostics;

namespace DesignPatterns.SingleResponsibility
{
    class SingleResponsibilityExample
    {
        public static void Example1()
        {
            var journal = new Journal();
            journal.AddEntry("I cried today");
            journal.AddEntry("I ate a bug");
            Console.WriteLine(journal);

            var persistence = new Persistence();
            var filename = @"C:\Temp\journal.txt";
            persistence.SaveToFile(journal, filename, true);
            Process.Start(filename);
        }
    }
}