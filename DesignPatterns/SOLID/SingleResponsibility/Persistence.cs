﻿using System.IO;

namespace DesignPatterns.SingleResponsibility
{
    internal class Persistence 
    {
        public void SaveToFile(Journal journal, string filename, bool overwrite = false)
        {
            if (overwrite || !File.Exists(filename))
            {
                File.WriteAllText(filename, journal.ToString());
            }
        }
    }
}