﻿using System.Collections.Generic;

namespace DesignPatterns.DependencyInversion
{
    internal interface IRelationshipBrowser
    {
        IEnumerable<Person> FindAllChildren(string name);
    }
}