﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.DependencyInversion
{
    internal class Relationships : IRelationshipBrowser // low-level
    {
        private readonly List<(Person, Relationship, Person)> _relations = new List<(Person, Relationship, Person)>();

        public IEnumerable<Person> FindAllChildren(string name)
        {
            return Enumerable.Where<ValueTuple<Person, Relationship, Person>>(_relations, r => r.Item1.Name == name
                            && r.Item2 == Relationship.Parent)
                .Select(r => r.Item3);
        }


        public void AddParentAndChild(Person parent, Person child)
        {
            _relations.Add((parent, Relationship.Parent, child));
            _relations.Add((child, Relationship.Child, parent));
        }
    }
}