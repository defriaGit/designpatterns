﻿namespace DesignPatterns.DependencyInversion
{

    // High level parts of a system should not dependent on low level parts of the system
    // they should depend on some abstraction 

    internal class ExampleDependencyInversion
    {
        public static void Example1()
        {
            var parent = new Person {Name = "John"};
            var child1 = new Person {Name = "Chris"};
            var child2 = new Person {Name = "Mike"};

            var relationships = new Relationships();
            relationships.AddParentAndChild(parent, child1);
            relationships.AddParentAndChild(parent, child2);

            var research = new Research(relationships);
        }
    }
}