﻿namespace DesignPatterns.DependencyInversion
{
    internal enum Relationship
    {
        Parent,
        Child,
        Sibling
    }
}