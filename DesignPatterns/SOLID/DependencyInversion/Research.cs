﻿using System;

namespace DesignPatterns.DependencyInversion
{
    internal class Research
    {
        public Research(IRelationshipBrowser browser)
        {
            foreach (var relation in browser.FindAllChildren("John"))
                Console.WriteLine($"John has a child called {relation.Name}");
        }
    }
}