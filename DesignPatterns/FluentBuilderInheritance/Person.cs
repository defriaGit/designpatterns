﻿namespace DesignPatterns.FluentBuilderInheritance
{
    class Person
    {
        public string Name;
        public string Position;

        public class Builder : PersonJobBulder<Builder>
        {

        }

        public static Builder New => new Builder();

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Position)}: {Position}";
        }
    }
}