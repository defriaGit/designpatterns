﻿namespace DesignPatterns.FluentBuilderInheritance
{
    class PersonJobBulder<TSelf> : PersonInfoBuilder<PersonJobBulder<TSelf>> 
        where TSelf : PersonJobBulder<TSelf>
    {
        public  TSelf WorksAsA(string position)
        {
            Person.Position = position;
            return (TSelf) this;

        }
    }
}