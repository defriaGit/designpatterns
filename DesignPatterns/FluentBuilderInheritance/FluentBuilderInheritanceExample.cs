﻿using System;

namespace DesignPatterns.FluentBuilderInheritance
{
    class FluentBuilderInheritanceExample
    {
        public static void Example1()
        {
            var me = Person.New.Called("Defria")
                .WorksAsA("quant").Build();

            Console.WriteLine(me);
        }

        public static void SHowType()
        {
            PersonJobBulder<Person.Builder> personJobBulder = Person.New.Called("Defria");
        }
    }
}