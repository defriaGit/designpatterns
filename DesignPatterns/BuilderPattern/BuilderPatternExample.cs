﻿using System;
using System.Text;

namespace DesignPatterns.BuilderPattern
{
    class BuilderPatternExample
    {
        public static void Example1()
        {
            var hello = "hello";

            var sb = new StringBuilder();
            sb.Append("<p>");
            sb.Append("Hello");
            sb.Append("</p>");
            Console.WriteLine(sb);

            var words = new[] { "Hello", "World" };
            sb.Clear();
            sb.Append("<ul>");
            foreach (var word in words)
                sb.AppendFormat($"<li>{word}</li>");
            sb.Append("</ul>");
            Console.WriteLine(sb);


            var bulder = new HtmlBuilder("ul")
            .AddChild("li", "hello")
            .AddChild("li", "world")
            .AddChild("li", "Yeah");
            Console.WriteLine(bulder);
            Console.WriteLine();
        }
    }
}