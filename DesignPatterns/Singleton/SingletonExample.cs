﻿using System;

namespace DesignPatterns.Singleton
{
    public class SingletonExample
    {
        public static void Example1()
        {
            Repository.FileReader = new FileReader();
            var db = Repository.Instance;
            var city = "Tokyo";
            Console.WriteLine($"{city} has population of {db.GetPopulation(city)}");
        }
    }
}