﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MoreLinq;

namespace DesignPatterns.Singleton
{
    public class Repository : IRepository
    {
        private Dictionary<string, int> _capitals;
        private static int instanceCount;
        public static int Count => instanceCount;

        public static IFileReader FileReader { get; set; }

        public Repository(Dictionary<string, int> capitals)
        {
            _capitals = capitals;
        }

        private Repository(IFileReader fileReader = null)
        {
            instanceCount++;
            Console.WriteLine("Initializing database");
            _capitals = FileReader.Read(@".\Singleton\capitals.txt").Batch(2).ToDictionary(
                list => list.ElementAt(0).Trim(),
                list => int.Parse(list.ElementAt(1)));
        }

        public int GetPopulation(string name)
        {
            return _capitals[name];
        }

        private static Lazy<Repository> _instance = new Lazy<Repository>(() => new Repository());

        public static Repository Instance => _instance.Value; 
    }
}