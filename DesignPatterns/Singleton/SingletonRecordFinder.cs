﻿using System.Collections.Generic;

namespace DesignPatterns.Singleton
{
    public class SingletonRecordFinder
    {
        public int GetTotalPopulation(IEnumerable<string> names)
        {
            int result = 0;
            foreach (var name in names)
                result += Repository.Instance.GetPopulation(name);
            return result;
        }
    }
}