﻿using System;

namespace DesignPatterns.Singleton.Monostate
{
    public class CEOExample
    {
        public static void Example1()
        {
            var ceo = new CEO();
            ceo.Name = "John Doe";
            ceo.Age = 35;

            var ceo2 = new CEO();
            Console.WriteLine(ceo2);
        }
    }
}