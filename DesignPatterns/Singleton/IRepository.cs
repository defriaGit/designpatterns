﻿namespace DesignPatterns.Singleton
{
    public interface IRepository : IDatabase
    {
        int GetPopulation(string name);
    }
}