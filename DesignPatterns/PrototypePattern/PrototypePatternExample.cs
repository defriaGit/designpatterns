﻿using System;
using DesignPatterns.PrototypePattern;

namespace DesignPatterns.PrototypePattern
{
    class PrototypePatternExample
    {
        public static void Example1()
        {
            var person = new Person(new[] {"John", "Doe"}, new Address("London Road", 123));
            var john = person.DeepCopyXml();
            john.Names[0] = "Jane";
            john.Address.HouseNumber = 321;
            Console.WriteLine(person);
            Console.WriteLine(john);
        }
    }
}