﻿namespace DesignPatterns.PrototypePattern
{
    public class Address
    {
        public string StreetName;
        public int HouseNumber;
        private Address address;

        public Address()
        {
        }

        public Address(Address address)
        {
            this.StreetName = address.StreetName;
            HouseNumber = address.HouseNumber;
        }

        public Address(string streetName, int houseNumber)
        {
            StreetName = streetName;
            HouseNumber = houseNumber;
        }


        public override string ToString()
        {
            return $"{nameof(StreetName)}: {StreetName}, {nameof(HouseNumber)}: {HouseNumber}";
        }
    }
}